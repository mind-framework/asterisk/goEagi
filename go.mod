module github.com/andrewyang17/goEagi

go 1.16

require (
	cloud.google.com/go/speech v1.0.0
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/cryptix/wav v0.0.0-20180415113528-8bdace674401
	github.com/zaf/agi v0.0.0-20170121130222-38a15db6691c
	golang.org/x/net v0.0.0-20210805182204-aaa1db679c0d // indirect
	google.golang.org/genproto v0.0.0-20210924002016-3dee208752a0
)
